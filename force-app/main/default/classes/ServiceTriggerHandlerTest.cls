@IsTest
public class ServiceTriggerHandlerTest {
   @Testsetup
    public static void setup(){
        testfactory.testServiceTrigger();
    }
    
    @IsTest
    public static void serviceTest(){
        List<Program_Enrollment__c> peLIst =[SELECT ID,Contact__c FROM Program_Enrollment__c];
        Service__c ser = (Service__c)TestFactory.createSObject(new Service__c(Program_Enrollment__c = peLIst.get(0).id),'TestFactory.ServiceDefaults', true);
        
        List<Program_Enrollment__c> programEnrollmentList =[SELECT ID,Contact__c FROM Program_Enrollment__c WHERE ID =: ser.Program_Enrollment__c ];

		List<Id> contactIds = new List<Id>();

        for(Program_Enrollment__c pr : programEnrollmentList){
            contactIds.add(pr.Contact__c);                
        }
        
        List<Contact> contactList = [SELECT ID,Last_Service_Date__c,AccountId FROM Contact WHERE ID IN: contactIds];
        List<Account> alistToUpdate = [SELECT ID,Last_Service_Date__c FROM Account WHERE ID =: contactList.get(0).AccountId ];
        
        system.assertEquals(contactList.get(0).Last_Service_Date__c, alistToUpdate.get(0).Last_Service_Date__c);
        
    }
    
        @IsTest
    public static void serviceTest_1(){
        List<Program_Enrollment__c> peLIst =[SELECT ID,Contact__c FROM Program_Enrollment__c];
        Service__c ser = (Service__c)TestFactory.createSObject(new Service__c(Program_Enrollment__c = peLIst.get(0).id),'TestFactory.ServiceDefaults', true);

        
        List<Program_Enrollment__c> programEnrollmentList =[SELECT ID,Contact__c FROM Program_Enrollment__c WHERE ID =: ser.Program_Enrollment__c ];

		List<Id> contactIds = new List<Id>();

        for(Program_Enrollment__c pr : programEnrollmentList){
            contactIds.add(pr.Contact__c);                
        }
        
        List<Contact> contactList = [SELECT ID,Last_Service_Date__c,AccountId FROM Contact WHERE ID IN: contactIds];
        List<Account> alistToUpdate = [SELECT ID,Last_Service_Date__c FROM Account WHERE ID =: contactList.get(0).AccountId ];
        
        system.assertEquals(contactList.get(0).Last_Service_Date__c, alistToUpdate.get(0).Last_Service_Date__c);
        
    }

}