//Provisio Partners
//Developer: Yuliia Artemenko <yuliia.artemenko@redtag.com.ua>
//Date: 16/09/2019
//Description:
//This is a Trigger Handler that process related Bed Assignment records.

public class BedAssignmentTriggerHandler {

	public static void entry(TriggerParams triggerParams) {

		List<Bed_Assignment__c> triggerNew = (List<Bed_Assignment__c>) triggerParams.triggerNew;
		List<Bed_Assignment__c> triggerOld = (List<Bed_Assignment__c>) triggerParams.triggerOld;
		Map<Id, Bed_Assignment__c> oldMap = (Map<Id, Bed_Assignment__c>) triggerParams.oldMap;
		Map<Id, Bed_Assignment__c> newMap = (Map<Id, Bed_Assignment__c>) triggerParams.newMap;
		
		if (triggerParams.isBefore) {
			if (triggerParams.isInsert) {
				validationBeforeAssignment(triggerNew);
			}
            if (triggerParams.isUpdate) {
				validationBeforeAssignment(triggerNew);
			}
		}
	}

    public static void validationBeforeAssignment(List<Bed_Assignment__c> triggerNew){

        List<Bed_Assignment__c> assignmentRecords = new List<Bed_Assignment__c>();
        for (Bed_Assignment__c bedAssignment : triggerNew) {
            if (bedAssignment.Assigned__c == true) {
                assignmentRecords.add(bedAssignment);
            }
        }

        if(!assignmentRecords.isEmpty()){

            Map<Id, Bed_Assignment__c> newAssignments = new Map<Id, Bed_Assignment__c>();
            Set<Id> setOfBedIds =  new Set<Id>();

            for(Bed_Assignment__c assignment: assignmentRecords){
                if(!newAssignments.containsKey(assignment.Bed__c)){
                    newAssignments.put(assignment.Bed__c, assignment);
                    setOfBedIds.add(assignment.Bed__c);
                }else{
                    assignment.addError(Label.Bed_Assignment_Error_1);
                }
            }
            
            String bedAssignmentQuery = ObjectUtils.getAllFieldsFromObject('Bed_Assignment__c') + ' WHERE Bed__c in :setOfBedIds';

            List<Bed_Assignment__c> previousBedAss = Database.query(bedAssignmentQuery);

            if(!previousBedAss.isEmpty()){

                Map<Id, List<Bed_Assignment__c>> mapOfpreviousBedAss = new Map<Id, List<Bed_Assignment__c>>();
                Map<Id, Bed_Assignment__c> uniqueAssignments = new Map<Id, Bed_Assignment__c>();
                Map<Id, Bed_Assignment__c> mapOfCurrentBedAss = new Map<Id, Bed_Assignment__c>();

                for(Bed_Assignment__c previousAss: previousBedAss){
                    if(!mapOfpreviousBedAss.containsKey(previousAss.Bed__c)){
                        mapOfpreviousBedAss.put(previousAss.Bed__c, new List<Bed_Assignment__c>());
                    }
                    mapOfpreviousBedAss.get(previousAss.Bed__c).add(previousAss);
                }

                //get unique Bed Assignments for next validations
                for(Id bedId: newAssignments.keySet()){
                    if(!mapOfpreviousBedAss.containsKey(bedId)){
                        uniqueAssignments.put(bedId, newAssignments.get(bedId));
                    }else{
                        mapOfCurrentBedAss.put(bedId, newAssignments.get(bedId));
                    }
                }

                if(!uniqueAssignments.isEmpty()){
                    checkRelatedBeds(uniqueAssignments);
                }
                //check active Assignments
                checkActAssToEachAssignment(mapOfCurrentBedAss, mapOfpreviousBedAss);
            }else{
                checkRelatedBeds(newAssignments);
            }  
        }
    }

    //check related Beds to one Room
    public static void checkRelatedBeds(Map<Id, Bed_Assignment__c> newAssignments){

        //get list of Bed Assignments
        Set<Id> setOfBedIds =  new Set<Id>();
        setOfBedIds.addAll(newAssignments.keySet());
        String bedQuery = ObjectUtils.getAllFieldsFromObject('Bed__c') + ' WHERE Id in :setOfBedIds';
		System.debug(bedQuery);
		List<Bed__c> availableBeds = Database.query(bedQuery);

        //get rooms based on Bed Assignments
        Set<Id> roomIds = new Set<Id>(); 
        for(Bed__c bed:  availableBeds){
            roomIds.add(bed.Room__c);
        }

        //get Assigned assignments to other beds in one room
        String relatedBedQuery = ObjectUtils.getAllFields('Bed_Assignment__c') + ', Bed__r.Room__c FROM Bed_Assignment__c WHERE Assigned__c = TRUE AND Bed__r.Room__c in :roomIds'; 

        List<Bed_Assignment__c> relatedBedAssignments = Database.query(relatedBedQuery);

        //create map of Assigned Contact to one Room
        Map<Id, List<Contact>> mapOfRelatedAssCon = new Map<Id, List<Contact>>();

        Set<Id> setOfRelContacts = new Set<Id>();
        for(Bed_Assignment__c relAssign: relatedBedAssignments){
            setOfRelContacts.add(relAssign.Client__c);
        }

        List<Contact> listOfRealtedCon = [SELECT Id, Gender__c, Age__c, 
                                            (SELECT Id, npe4__RelatedContact__c, npe4__Contact__c, npe4__Type__c 
                                            FROM npe4__Relationships__r where npe4__Type__c ='Sibling')
                                        FROM Contact where Id IN: setOfRelContacts];
       
        for(Bed_Assignment__c relAss: relatedBedAssignments){
            for(Contact relCon: listOfRealtedCon){
                if(relAss.Client__c == relCon.Id){
                    if(!mapOfRelatedAssCon.containsKey(relAss.Bed__r.Room__c)){
                        mapOfRelatedAssCon.put(relAss.Bed__r.Room__c, new List<Contact>());
                    }
                    mapOfRelatedAssCon.get(relAss.Bed__r.Room__c).add(relCon);
                }
            }
        }

        //create map of Current Contact Assignments to one Room
        Map<Id, Contact> mapOfCurrentAssCon = new Map<Id, Contact>();
        Set<Id> setOfCurContacts = new Set<Id>();

        for(Bed_Assignment__c curAss: newAssignments.values()){
            setOfCurContacts.add(curAss.Client__c);
        }
        List<Contact> listOfCurCon = [SELECT Id, Gender__c, Age__c, 
                                            (SELECT Id, npe4__RelatedContact__c, npe4__Contact__c, npe4__Type__c 
                                            FROM npe4__Relationships__r where npe4__Type__c ='Sibling')
                                    FROM Contact where Id IN: setOfCurContacts];

    
        Map<Id, Id> bedToRoom = new Map<Id, Id>();
        for(Bed__c bed: availableBeds){
            bedToRoom.put(bed.Id, bed.Room__c);
        }

        for(Bed_Assignment__c curAss: newAssignments.values()){
            for(Contact curCon: listOfCurCon){
                if(curAss.Client__c == curCon.Id && bedToRoom.containsKey(curAss.Bed__c)){
                    mapOfCurrentAssCon.put(bedToRoom.get(curAss.Bed__c), curCon);
                }
            }
        }
        Set<Id> badAssignments = checkEachAssignment(mapOfCurrentAssCon, mapOfRelatedAssCon);

        //avoid bad Bed Assignments, return errors and inser good Bed Assignments  
        List<Bed_Assignment__c> toInsert = new List<Bed_Assignment__c>();

        for(Bed_Assignment__c newAss: newAssignments.values()){
            if(bedToRoom.containsKey(newAss.Bed__c)){
                if(badAssignments.contains(bedToRoom.get(newAss.Bed__c))){
                    newAss.addError(Label.Bed_Assignment_Error_3);
                }else{
                    toInsert.add(newAss);
                }
            }
        } 
    }

    //check for sibling, gender, age
    public static Set<Id> checkEachAssignment(Map<Id, Contact> mapOfCurrentAssCon, Map<Id, List<Contact>> mapOfRelatedAssCon){

        //create map of Current Contact Relationships  
        Map<Id, Set<Id>> mapOfCurConRelation = new Map<Id, Set<Id>>();
        for (Contact c : mapOfCurrentAssCon.values()) {
            for(npe4__Relationship__c r : c.npe4__Relationships__r) {
                if(!mapOfCurConRelation.containsKey(c.Id)){
                    mapOfCurConRelation.put(c.Id, new Set<Id>());
                }
                mapOfCurConRelation.get(c.Id).add(r.npe4__RelatedContact__c);
            }
        }
        
        Set<Id> badAssignments =  new Set<Id>();
        //check for sibling, gender, age
        for(Id roomId: mapOfCurrentAssCon.keySet()){
            if(mapOfRelatedAssCon.containsKey(roomId)){
                Contact currentContact = mapOfCurrentAssCon.get(roomId);
                Boolean goodAssign = true;
                for(Contact relContact: mapOfRelatedAssCon.get(roomId)){
                    goodAssign = validateEachBedAssignment(mapOfCurConRelation, currentContact, relContact, goodAssign);
                }

                if(goodAssign == false) {
                    badAssignments.add(roomId);
                }
            }
        }
                                            
        return badAssignments;
    }

    public static Boolean validateEachBedAssignment(Map<Id, Set<Id>> mapOfCurConRelation, Contact currentContact, Contact relContact, Boolean goodAssign){

        if(!mapOfCurConRelation.isEmpty()){
            if((!mapOfCurConRelation.get(currentContact.Id).contains(relContact.Id)) 
                    && ((6 <= currentContact.Age__c  && currentContact.Age__c<=12) 
                    && (6 <= relContact.Age__c && relContact.Age__c <=12) 
                    && (currentContact.Gender__c != relContact.Gender__c))){
                goodAssign = false;
            }
        }else{
            if((6 <= currentContact.Age__c  && currentContact.Age__c<=12) 
                    && (6 <= relContact.Age__c && relContact.Age__c <=12) 
                    && (currentContact.Gender__c != relContact.Gender__c)){
                goodAssign = false;
            }
        }
        return goodAssign;
    }

    public static void checkActAssToEachAssignment(Map<Id, Bed_Assignment__c> mapOfCurrentBedAss, Map<Id, List<Bed_Assignment__c>> mapOfpreviousBedAss){

        Set<Id> badAssignments =  new Set<Id>();

        for(Id bedId: mapOfCurrentBedAss.keySet()){
            if(mapOfpreviousBedAss.containsKey(bedId)){
                Boolean goodAssign = true;
                for(Bed_Assignment__c prevAss: mapOfpreviousBedAss.get(bedId)){
                    if(prevAss.Assigned__c == true){
                        goodAssign = false;
                    }
                }
                if(goodAssign == false){
                    badAssignments.add(bedId);
                }
            }
        }
        //avoid bad Bed Assignments, return errors and move forward with good Assignments

        Map<Id, Bed_Assignment__c> goodAssignments = new Map<Id, Bed_Assignment__c>();
        for(Id bedId: mapOfCurrentBedAss.keySet()){
            if(badAssignments.contains(bedId)){
                mapOfCurrentBedAss.get(bedId).addError(Label.Bed_Assignment_Error_2);
            }else{
                goodAssignments.put(bedId, mapOfCurrentBedAss.get(bedId));
            }
        }
        if(!goodAssignments.isEmpty()){
            checkRelatedBeds(goodAssignments);
        }
    }
}