@isTest
public class BatchExceptionLogSendEmailTest  {

	@testSetup static void initialize() {
        TestFactory.createDataForBatchExceptionLogSendEmail();
    }

	@isTest
    static void startBatchExceptionlogSendEmailTest() {
		Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
		User testUser = (User) TestFactory.createSObject(new User(ProfileId = p.Id), 'TestFactory.UserDefaults', true);
		List<Group> groupExceptionReceivers = [SELECT Id, name FROM Group WHERE DeveloperName =: GlobalVariable.EXCEPTION_RECEIVERS_GROUP LIMIT 1];
		if(groupExceptionReceivers.isEmpty()){
			groupExceptionReceivers.add((Group) TestFactory.createSObject(new Group(Name = 'Address group'), true));
		}
		GroupMember groupMember = (GroupMember) TestFactory.createSObject(new GroupMember(UserOrGroupId = testUser.Id, GroupId = groupExceptionReceivers.get(0).Id), true);

        Test.startTest();
		BatchExceptionLogSendEmail obj = new BatchExceptionLogSendEmail();
		DataBase.executeBatch(obj); 
        Test.stopTest();
    }
}