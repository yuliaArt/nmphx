global class BatchScheduleExceptionLogSendEmail implements Schedulable {
	
	global void execute(SchedulableContext sc) {
		proccessSchedule();
	}

	private void proccessSchedule() {
		List<id> usersIdFromPublicGroup = ObjectUtils.getUsersIdFromPublicGroup(GlobalVariable.EXCEPTION_RECEIVERS_GROUP);
		System.debug('usersIdFromPublicGroup '+usersIdFromPublicGroup );
		//Check if exist any user in group
		if(!usersIdFromPublicGroup.isEmpty()){
			String query = ObjectUtils.getAllFieldsFromObject('Exception_Log__c') + ' WHERE IsSended__c = false';
			List<Exception_Log__c> ExceptionLogList = Database.query(query);

			//Check if exist any records
			if (!ExceptionLogList.isEmpty()) {
				BatchExceptionLogSendEmail batchExceptionLog = new BatchExceptionLogSendEmail();
				
				//Check if there are less than 5 batches active, otherwise wait for 5 minutes and try again
				Integer numberOfBatch = [SELECT count() FROM AsyncApexJob WHERE JobType = 'BatchApex' AND Status IN('Processing', 'Preparing', 'Queued')];
				if (numberOfBatch < 5) {
					Database.executeBatch(batchExceptionLog, 200);
				} 
				if (numberOfBatch >= 5 || Test.isRunningTest()) {				
					BatchScheduleExceptionLogSendEmail delayBachExceptionLog = new BatchScheduleExceptionLogSendEmail();
					Datetime dt = Datetime.now().addMinutes(5);
					String timeForScheduler = dt.format('s m H d M \'?\' yyyy');
					Id schedId = System.Schedule('Send Exception Log Email Batch Schedule' + timeForScheduler, timeForScheduler, delayBachExceptionLog );
				}
			
			}
		}
	}

	/* At 17:00:00pm every day
		BatchScheduleExceptionLogSendEmail batchScheduleExceptionLogSendEmail = new BatchScheduleExceptionLogSendEmail();
		String sch = '0 0 17 ? * * *';
		system.schedule('Send Exception Log in Email', sch, batchScheduleExceptionLogSendEmail);
	*/
}