//**
// Author: Tejashree Deshpande
// Description: Controller to display Goal, Care plan and action step details
// Revised: 
//      Author: Ivanna Kuzemchak
//      Date: December 13, 2018
//      Description: Class was modified because of difference in data model.
//
// This code is the property of Provisio Partners and copy or reuse is prohibited.
// Copyright @2018 Provisio Partners Illinois Ltd. All Rights Reserved.
// support@provisiopartners.org
//**
public with sharing class ServicePlanController {
	public static ParentWrapper records {get; set;}

	public ServicePlanController(){
		String recordId = ApexPages.currentPage().getParameters().get('recordId');
		records = getRecords(recordId);
	}
	
	@AuraEnabled
	public static ParentWrapper getRecords(String recordId){
		
		List<Service_Plan__c> servicePlans = [SELECT Id,
													 Name,
													 Program_Enrollment__r.Name,
													 Program_Enrollment__r.Contact__r.Full_Name__c,
													 Start_Date__c,											
													 End_Date__c,
													 Target_Date__c,
													 Closure_Reason__c,
													 Notes__c,
													 Barriers__c,
													 Active_Goals__c,
													 RecordTypeId,
													 RecordType.Name,
													 CreatedBy.Name
											  FROM Service_Plan__c
											  WHERE Id =: recordId
		];

		Map<Id, Goal__c> goalsById = new Map<Id, Goal__c>([SELECT id,
																  Name,
																  Service_Plan__c,
																  Start_Date__c,
																  End_Date__c,
																  Active__c,
																  Outcome__c,
																  Notes__c
														   FROM Goal__c 
														   WHERE Service_Plan__c =: recordId
		]); 
		 
		List<Action_Step__c> actionSteps = [SELECT id, 
												   Name,  
												   Start_Date__c,
												   Completion_Date__c,
												   Notes__c,
												   Due_Date__c,
												   Goal__c
											FROM Action_Step__c 
											WHERE Goal__c =: goalsById.keySet()
		];

		ParentWrapper parentWarpper = new ParentWrapper();
		parentWarpper.parentObject = servicePlans.get(0);

		List<ChildWrapper> childWrappers = new List<ChildWrapper>();
		for (Id goalId: goalsById.keySet()) {
			ChildWrapper childWrapper = new ChildWrapper();
			childWrapper.childObject = goalsById.get(goalId);
			List<SObject> nextChilds = new List<SObject>();
			for (Action_Step__c actionStep: actionSteps) {
				if(actionStep.Goal__c == goalId) {
					nextChilds.add(actionStep);
				}
			}
			childWrapper.nextChilds = nextChilds;
			childWrappers.add(childWrapper);
		}
		parentWarpper.childs = childWrappers;
		
		return parentWarpper;
	}

	public class ParentWrapper {
		@AuraEnabled public SObject parentObject {get;set;}
		@AuraEnabled public List<ChildWrapper> childs {get; set;}
	}

	public class ChildWrapper {
		@AuraEnabled public SObject childObject {get;set;}
		@AuraEnabled public List<SObject> nextChilds {get;set;}
	}
}