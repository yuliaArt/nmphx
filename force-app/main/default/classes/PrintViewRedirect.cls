public with sharing class PrintViewRedirect {
    public String recordId {get; set;}
    
    public PrintViewRedirect(ApexPages.StandardController controller){
        this.recordId= getrecordId(controller);
    }
    
    public Id getrecordId(ApexPages.StandardController controller)
    {
        return controller.getId();
    }
}