//**
// This code is the property of Provisio Partners and copy or reuse is prohibited.
// Copyright @2018 Provisio Partners Illinois Ltd. All Rights Reserved.
// support@provisiopartners.org
//**

public class ServiceWrapper {
    @AuraEnabled public Date dateOfService;
    @AuraEnabled public Integer Hours;
    @AuraEnabled public Integer Minutes;
    @AuraEnabled public String notes;
    @AuraEnabled public String program;
    @AuraEnabled public Integer noOfAdult;
    @AuraEnabled public String Counselor;
    @AuraEnabled public Boolean Transportation;
    @AuraEnabled public Boolean food;
    @AuraEnabled public Boolean CaseManagementServices;
    @AuraEnabled public Boolean LifeSkillsGroup;
    @AuraEnabled public Boolean EmploymentServices;
    @AuraEnabled public Integer duration;
    @AuraEnabled public String otherTopic;
    
    public ServiceWrapper(){
        dateOfService = Date.today();
        Hours = 0;
        Minutes = 0;
        notes = '';
        program = null;
        noOfAdult = 0;
        Counselor = '';
        Transportation = false;
        food=false;
        CaseManagementServices = false;
        LifeSkillsGroup = false;
        EmploymentServices = false;
        duration = 0;
        otherTopic = '';
    }
}