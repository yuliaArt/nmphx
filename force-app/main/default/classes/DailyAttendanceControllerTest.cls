/* Provisio Partners
** Author: Dnyaneshwar Waghmare
** Date: 08/20/2018
** Description: Test class for Daily Attendance Controller test.     
**/

@IsTest
public class DailyAttendanceControllerTest {

    @TestSetup
    public static void setup(){
        TEstFactory.createDataForAttendanceController();
    }

    public static  testmethod void attendancetestMethod(){
        
        String datestring = String.valueOf(Date.today().toStartOfWeek().addDays(2));
        List<Class_Roster__c> crList = [SELECT ID FROM Class_Roster__c]; 
        //List<Attendance__c> attendanceList = TestFactory.createSObjectList(new Attendance__c(Class_Roster__c = crList[0].Id),10,'TestFactory.AttendanceDefaults', true);               
		List<Attendance__c> attendanceList = [SELECT Id FROM Attendance__c];
        system.assert(attendanceList.size() == 4);
        
        Test.startTest();
        DailyAttendanceController.serverGetAttendanceRecords(crList[0].Id, datestring);
        DailyAttendanceController.serverUpdateAttendanceRecord(attendanceList);
        Test.stopTest();
    }
}