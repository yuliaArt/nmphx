@isTest
private class PrintViewRedirectTest {
    @TestSetup
    static void testSetup(){
        TestFactory.createDataForPrintableViewServicePlanControllerAndRedirectPage();
    }
    @isTest
    static void testRedirectPage() {
    Service_Plan__c servicePlan = [SELECT Id FROM Service_Plan__c LIMIT 1];
    ApexPages.StandardController sc= new ApexPages.StandardController(servicePlan);
    PrintViewRedirect pvRed = new PrintViewRedirect(sc);
    }
}