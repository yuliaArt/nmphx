@isTest 
private class GetFieldTypeControllerTest {

	@isTest
	private static void getFieldTypeTest() {
		String fieldType = GetFieldTypeController.getFieldType('Account', 'Name');
		System.assertEquals('STRING', fieldType);
	}
}