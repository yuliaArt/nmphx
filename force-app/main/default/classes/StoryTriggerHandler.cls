//*
// Author: Ivanna Kuzemchak
// Date: July 26, 2019
// Description: Trigger Handler for Story 
// This code is the property of Provisio Partners and copy or reuse is prohibited.
// Copyright @2019 Provisio Partners Illinois Ltd. All Rights Reserved.
// support@provisiopartners.org
//*
//
public class StoryTriggerHandler {
    
	public static void entry(TriggerParams triggerParams) {

		List<Story__c> triggerNew = (List<Story__c>) triggerParams.triggerNew;
		List<Story__c> triggerOld = (List<Story__c>) triggerParams.triggerOld;
		Map<Id, Story__c> oldMap = (Map<Id, Story__c>) triggerParams.oldMap;
		Map<Id, Story__c> newMap = (Map<Id, Story__c>) triggerParams.newMap;

		if (triggerParams.isBefore) {
			if (triggerParams.isInsert) {
		
			}
            if (triggerParams.isUpdate) {
 				shareRecordUpdate(oldMap, newMap);
			}
		}
		if (triggerParams.isAfter) {
			if (triggerParams.isInsert) {
				shareRecordInsert(triggerNew);
			}
			if (triggerParams.isUpdate) {
 
			}
		}
	}
    
    public static void shareRecordInsert(List<Story__c> triggerNew) {
        
        List<Story__c> storyList = new List<Story__c>();
        for (Story__c str : triggerNew) {
            if (str.Share__c == true) {
                storyList.add(str);
            }
        }
        if (!storyList.isEmpty()) {
            shareRecord(storyList);
        }
        
    }
    public static void shareRecordUpdate(Map<Id, Story__c> oldMap, Map<Id, Story__c> newMap) {
        
        List<Story__c> storyList = new List<Story__c>();
        for (Story__c str : newMap.values()) {
            if (oldMap.get(str.Id).Share__c == false && newMap.get(str.Id).Share__c == true) {
                storyList.add(str);
            }
        }
        if (!storyList.isEmpty()) {
            shareRecord(storyList);
        }
    }
    
    
    public static void shareRecord(List<Story__c> storyList) {
                
        /* Select Users with the highest role */
       	List<User> topUsers = [Select Id, UserRoleId FROM User WHERE UserRoleId In (Select Id from UserRole where ParentRoleId = Null)];
        Set <Id> topUsersIds = new Set<Id>();
        
        for (User usr : topUsers) {
            topUsersIds.add(usr.UserRoleId);
        }
        
        /* Select Group where listed all org users */
        List<Group> groupList = [SELECT Id, RelatedId, DeveloperName FROM Group 
                                 WHERE RelatedId in :topUsersIds AND Type = 'RoleAndSubordinatesInternal'];
            
        List<Story__Share> sharesList = new List<Story__Share>();
        
        for (Story__c str : storyList) {
            for (Group gr : groupList) {
                
                Story__Share storySHR = new Story__Share();
                storySHR.ParentId = str.Id;
                storySHR.AccessLevel = 'Read';
                storySHR.UserOrGroupId = gr.Id;
                sharesList.add(storySHR);
                
            }
        }
        
        if(!sharesList.isEmpty()) {
            insert sharesList;
		}
               
    }
    
}