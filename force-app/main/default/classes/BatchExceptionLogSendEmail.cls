//**
// Author: Mykhailo
// Date: April 01, 2019
// Description: This is a Batch Class that send email every day with Exception log records that were created a day.
//
// This code is the property of Provisio Partners and copy or reuse is prohibited.
// Copyright @2019 Provisio Partners Illinois Ltd. All Rights Reserved.
// support@provisiopartners.org
//**
global class BatchExceptionLogSendEmail implements Database.Batchable<SObject>, Database.Stateful {
	public String attachmentBody = '';

	global Database.QueryLocator start(Database.BatchableContext context) {
		String query = ObjectUtils.getAllFieldsFromObject('Exception_Log__c') + ' WHERE IsSended__c = false';
		return Database.getQueryLocator(query);
	}

	global void execute(Database.BatchableContext context, List<Exception_Log__c> scope) {
		attachmentBody += BatchExceptionLogSendEmailHelper.buildExceptionEmail(scope);
	}

	global void finish(Database.BatchableContext context) {
		BatchExceptionLogSendEmailHelper.sendEmails(attachmentBody);
	}
}