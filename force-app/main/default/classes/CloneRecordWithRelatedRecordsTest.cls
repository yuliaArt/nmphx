@isTest
public class CloneRecordWithRelatedRecordsTest {
        
    @testsetup
    public static void tesetup(){
        TestFactory.createDataForCloneRecordController();

    }

    @isTest
    public static void test1(){
        List<Service_plan__c> serList =[SELECT id from Service_plan__c];

        Test.Starttest();
           CloneRecordWithRelatedRecordsController.cloneMethod(serList[0].id);
        Test.StopTest();

        List<Service_plan__c> toAssert =[SELECT id from Service_plan__c];
        List<Goal__c> toAssert1 =[SELECT id from Goal__c];
        List<Action_Step__c> toAssert2 =[SELECT id from Action_Step__c];

        System.assertEquals(2,toAssert.size(),'cloned record here');
        System.assertEquals(2,toAssert1.size(),'cloned record here');
        System.assertEquals(2,toAssert2.size(),'cloned record here');
    }  
 }