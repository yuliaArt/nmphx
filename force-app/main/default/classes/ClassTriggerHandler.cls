//**
// Author: Mykhailo Senyuk
// Date: May 08, 2019
// Description: This is a Trigger Handler that process related Class Session records.
//		If Class record is created, this class creates Class Session records from start date to end date.
//		If Class record is updates, this class creates Class Session records or deletes unnecessery records.
//
// This code is the property of Provisio Partners and copy or reuse is prohibited.
// Copyright @2018 Provisio Partners Illinois Ltd. All Rights Reserved.
// support@provisiopartners.org
//**
public class ClassTriggerHandler {

	public static void entry(TriggerParams triggerParams) {

		List<Class__c> triggerNew = (List<Class__c>) triggerParams.triggerNew;
		List<Class__c> triggerOld = (List<Class__c>) triggerParams.triggerOld;
		Map<Id, Class__c> oldMap = (Map<Id, Class__c>) triggerParams.oldMap;
		Map<Id, Class__c> newMap = (Map<Id, Class__c>) triggerParams.newMap;
		
		if (triggerParams.isAfter) {
			if (triggerParams.isInsert) {
				createNewSessionRecord(newMap, null, false);
			}
			if (triggerParams.isUpdate) {
				updateSessionRecord(oldMap, newMap);
			}
		}

	}

	public static void createNewSessionRecord(Map<Id, Class__c> newClassById, Map<Id, Map<Date, Class_Session__c>> oldClassSessionsByDateByClassId, Boolean isDelete) {
		List<Class_Session__c> classSessions = new List<Class_Session__c> ();
		Map<Id, Map<Date, Class_Session__c>> sessionsToDeleteByDateByClassId = new Map<Id, Map<Date, Class_Session__c>> ();
		System.debug(oldClassSessionsByDateByClassId);
		if(oldClassSessionsByDateByClassId != null) {
			sessionsToDeleteByDateByClassId.putAll(oldClassSessionsByDateByClassId);
		}
		
		for (Id classId : newClassById.keySet()) {
			if (newClassById.get(classId).Start_Date__c != null && newClassById.get(classId).End_Date__c != null) {
				for (Date classDate = newClassById.get(classId).Start_Date__c; classDate <= newClassById.get(classId).End_Date__c; classDate = classDate.addDays(1)) {
					for (Integer day : ClassRosterTriggerHandler.listOfClassDays(newClassById.get(classId))) {
						if ( dayOfWeek(classDate) == day ) {
							System.debug(day);
							if( !isDelete && oldClassSessionsByDateByClassId != null && oldClassSessionsByDateByClassId.containsKey(classId) ){ //** when update class create Sessions
								System.debug('when update class create Sessions');
								if( !oldClassSessionsByDateByClassId.get(classId).containsKey(classDate) ){
									Class_Session__c classSession = new Class_Session__c();
									classSession.Class__c = classId;
									classSession.Date__c = classDate;
									classSessions.add(classSession);
								}
							} else if(!isDelete) { //** when create class create Sessions
								Class_Session__c classSession = new Class_Session__c();
								classSession.Class__c = classId;
								classSession.Date__c = classDate;
								classSessions.add(classSession);
							} else if(isDelete && sessionsToDeleteByDateByClassId.containsKey(classId)){ //** when update class delete Sessions
								sessionsToDeleteByDateByClassId.get(classId).remove(classDate);
							}
						}
					}
				}
			}
		}  
		if(isDelete){
			List<Class_Session__c> sessionsToDelete = new List<Class_Session__c>();
			
			for(Map<Date, Class_Session__c> sessionDate: sessionsToDeleteByDateByClassId.values()){
				sessionsToDelete.addAll(sessionDate.values());
			}
			if ( !sessionsToDelete.isEmpty() ) {
				Database.delete(sessionsToDelete, false);
			}
		}
		if (!classSessions.isEmpty()) {
			for(Class_Session__c clsSes: classSessions){
				System.debug(clsSes);
				Datetime dt = (Datetime) clsSes.Date__c;
				String dayOfWeekStr = dt.format('EEE');
				System.debug('dayOfWeekStr '+dayOfWeekStr);
			}
			insert classSessions;
		}

	}

	public static void updateSessionRecord(Map<Id, Class__c> oldClassById, Map<Id, Class__c> newClassById){
		System.debug('update');
		Map<Id, Map<Date, Class_Session__c>> oldClassSessionsByDateByClassId = new Map<Id, Map<Date, Class_Session__c>> ();
		
		List<Class_Session__c> oldClassSessions = [SELECT id, Date__c, Class__c FROM Class_Session__c WHERE Class__c in:oldClassById.keySet()];
		
		if (!oldClassSessions.isEmpty()) {
			for (Class_Session__c classSession : oldClassSessions) {
				if (oldClassSessionsByDateByClassId.containsKey(classSession.Class__c)) {
					Map<Date, Class_Session__c> sessionByDate = oldClassSessionsByDateByClassId.get(classSession.Class__c);
					sessionByDate.put(classSession.Date__c, classSession);
					oldClassSessionsByDateByClassId.put(classSession.Class__c, sessionByDate);
				} else {
					oldClassSessionsByDateByClassId.put(classSession.Class__c, new Map<Date, Class_Session__c> { classSession.Date__c => classSession });
				}
			}
		}

		for (Id classId: oldClassById.keySet()) {
			System.debug('class');
			if(oldClassById.get(classId).Start_Date__c != newClassById.get(classId).Start_Date__c || oldClassById.get(classId).End_Date__c != newClassById.get(classId).End_Date__c || oldClassById.get(classId).Monday__c != newClassById.get(classId).Monday__c || oldClassById.get(classId).Tuesday__c != newClassById.get(classId).Tuesday__c || oldClassById.get(classId).Wednesday__c != newClassById.get(classId).Wednesday__c || oldClassById.get(classId).Thursday__c != newClassById.get(classId).Thursday__c || oldClassById.get(classId).Friday__c != newClassById.get(classId).Friday__c || oldClassById.get(classId).Saturday__c != newClassById.get(classId).Saturday__c || oldClassById.get(classId).Sunday__c != newClassById.get(classId).Sunday__c ){
				System.debug('first if');
				if ( oldClassById.get(classId).Start_Date__c > newClassById.get(classId).Start_Date__c || oldClassById.get(classId).End_Date__c < newClassById.get(classId).End_Date__c || oldClassById.get(classId).End_Date__c != newClassById.get(classId).End_Date__c || oldClassById.get(classId).Monday__c != newClassById.get(classId).Monday__c || oldClassById.get(classId).Tuesday__c != newClassById.get(classId).Tuesday__c || oldClassById.get(classId).Wednesday__c != newClassById.get(classId).Wednesday__c || oldClassById.get(classId).Thursday__c != newClassById.get(classId).Thursday__c || oldClassById.get(classId).Friday__c != newClassById.get(classId).Friday__c  || oldClassById.get(classId).Saturday__c != newClassById.get(classId).Saturday__c || oldClassById.get(classId).Sunday__c != newClassById.get(classId).Sunday__c ){
					System.debug('update');
					createNewSessionRecord(newClassById, oldClassSessionsByDateByClassId, false); //update
				} 
				if ( oldClassById.get(classId).Start_Date__c < newClassById.get(classId).Start_Date__c || oldClassById.get(classId).End_Date__c > newClassById.get(classId).End_Date__c  || oldClassById.get(classId).Monday__c != newClassById.get(classId).Monday__c || oldClassById.get(classId).Tuesday__c != newClassById.get(classId).Tuesday__c || oldClassById.get(classId).Wednesday__c != newClassById.get(classId).Wednesday__c || oldClassById.get(classId).Thursday__c != newClassById.get(classId).Thursday__c || oldClassById.get(classId).Friday__c != newClassById.get(classId).Friday__c || oldClassById.get(classId).Saturday__c != newClassById.get(classId).Saturday__c || oldClassById.get(classId).Sunday__c != newClassById.get(classId).Sunday__c ) {
					System.debug('delete');
					createNewSessionRecord(newClassById, oldClassSessionsByDateByClassId, true); //delete
				}
				if ( newClassById.get(classId).Start_Date__c != null && newClassById.get(classId).End_Date__c != null && newClassById.get(classId).Start_Date__c != newClassById.get(classId).End_Date__c && (oldClassById.get(classId).Start_Date__c == null || oldClassById.get(classId).End_Date__c == null) ) {
					System.debug('create');
					createNewSessionRecord(newClassById, null, false);//create
				}
			}
		}
	}

	public static Integer dayOfWeek(Date d) {
		Datetime dt = (Datetime) d;
		String dayOfWeekStr = dt.format('EE');
		Map<String, Integer> dayOfWeekByName = new Map<String, Integer> {
			'Mon' => 1,
			'Tue' => 2,
			'Wed' => 3,
			'Thu' => 4,
			'Fri' => 5,
			'Sat' => 6,  
			'Sun' => 0 };
		return dayOfWeekByName.get(dayOfWeekStr);
	}
}