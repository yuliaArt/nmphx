/* Provisio Partners
** Author: Shashank Parashar
** Date: June 7th 2018
** Description:Test class for AttendanceWrapperController. Test class will updated soon.
*/
@IsTest
public class LookupControllerTest {
    static testmethod void testlookupcontroller(){
        String objectname = 'Group__c';
        String fieldApi = '';
        String fieldval = 'Name';
        Integer lim = 1;
        string fieldapisearch = 'Name';
        string searcht = 'test';

        //insert test class workshop record
        Group__c classworkshopRecord = new Group__c(Name = 'test', Start_Date__c=Date.today().addDays(1), End_Date__c=Date.today().addDays(10));
        insert classworkshopRecord;

        test.startTest();
        LookupController.getIconName('Group__c');
        LookupController.getObjectNameServer('Group__c');
        LookupController.searchDB(objectname, fieldApi, fieldval, fieldapisearch, lim);
        test.stopTest();
    }
}