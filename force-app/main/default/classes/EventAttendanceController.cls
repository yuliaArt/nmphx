//**
// Author: Yuliia Artemenko
// Date: Sep 11, 2019
// Description: Class works with EventAttendance Component, get/update Event Participant Records
//
// This code is the property of Provisio Partners and copy or reuse is prohibited.
// Copyright @2018 Provisio Partners Illinois Ltd. All Rights Reserved.
// support@provisiopartners.org
//**
public class EventAttendanceController {
    	//Method for fetching the records based on RecordId(Birdseye Event)
        @AuraEnabled
        public static List<Event_Participant__c> serverGetEventParticipantRecords(Id eventId) {

            List<EventAttendanceSetting__mdt> eventAttendanceSettings = eventAttendanceSettings();
            String eventAttendanceFields = '';
            for (Integer i = 0; i < eventAttendanceSettings.size(); i++){
                eventAttendanceFields += eventAttendanceSettings.get(i).Field_API_Name__c + ', ';
            }
            String query = 'SELECT Id, ' + eventAttendanceFields + ' Birdseye_Event__c, Name, Participant__r.Name FROM Event_Participant__c WHERE Birdseye_Event__c =: eventId ';
            List<Event_Participant__c> partList = Database.query(query);
            return partList;
        }

        //Method for Updating the records
        @AuraEnabled
        public static void serverUpdateParticipantRecord(List<Event_Participant__c> attendanceUpdate) {
            update attendanceUpdate;
        }

        //Method for geting Event Participant Settings from Custom matadata type
        @AuraEnabled
        public static List<EventAttendanceSetting__mdt> eventAttendanceSettings(){
            List<EventAttendanceSetting__mdt> eventAttendanceSettings = [SELECT id, MasterLabel, Field_API_Name__c, Object_API_Name__c, IsHeader__c, Order__c, IsVisible__c FROM EventAttendanceSetting__mdt WHERE IsVisible__c = TRUE ORDER BY Order__c];
            return eventAttendanceSettings;
        }
}