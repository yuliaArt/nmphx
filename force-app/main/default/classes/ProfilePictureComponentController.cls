public class ProfilePictureComponentController {
        
    @AuraEnabled
    public static Id getProfilePicture(Id parentId) {

        // Attachment permissions are set in parent object (Contact)
        if (!Schema.sObjectType.Contact.isAccessible()) {
            throw new System.NoAccessException();
        }
		Contact currentContact = [SELECT id, Profile_Picture_Id__c FROM Contact WHERE Id =: parentId];
		Attachment attachment = [SELECT id FROM Attachment WHERE Id =: currentContact.Profile_Picture_Id__c];

		return currentContact.Profile_Picture_Id__c;
 
        //return [SELECT Id, Name, LastModifiedDate, ContentType FROM Attachment 
            //WHERE parentid=:ParentId AND ContentType IN ('image/png', 'image/jpeg', 'image/gif') 
            //ORDER BY LastModifiedDate DESC LIMIT 1];
    }
    
    @AuraEnabled
    public static Id saveAttachment(Id parentId, String fileName, String base64Data, String contentType) { 

        // Edit permission on parent object (Contact) is required to add attachments
        if (!Schema.sObjectType.Contact.isUpdateable()) {
            throw new System.NoAccessException();
        }

        Attachment attachment = new Attachment();
        attachment.parentId = parentId;
        attachment.body = EncodingUtil.base64Decode(base64Data);
        attachment.name = fileName;
		attachment.contentType = contentType;
        insert attachment;

		Contact contact = new Contact();
		contact.Id = parentId;
		contact.Profile_Picture_Id__c = attachment.Id;
		update contact;

        return attachment.id;
    }


}