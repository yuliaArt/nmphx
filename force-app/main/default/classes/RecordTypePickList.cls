//**
// Author: Ivanna Kuzemchak
// Date: December 11, 2018
// Description: Class that is used to populate picklist for choosing RT when adding Lightning Component on page.
//
// This code is the property of Provisio Partners and copy or reuse is prohibited.
// Copyright @2018 Provisio Partners Illinois Ltd. All Rights Reserved.
// support@provisiopartners.org
//**

global class RecordTypePickList extends VisualEditor.DynamicPickList {

	global override VisualEditor.DataRow getDefaultValue(){
        VisualEditor.DataRow defaultValue = new VisualEditor.DataRow('None', 'None');
        return defaultValue;
    }
    global override VisualEditor.DynamicPickListRows getValues() {

    	VisualEditor.DataRow defaultValues = getDefaultValue();
    	List<RecordType> recordTypeList = [Select Id, Name from RecordType where SObjectType = 'Service__c'];
	
		//** only for test
		if(Test.isRunningTest()){
			for(Schema.RecordTypeInfo recorType: ObjectUtils.getAllRecordTypesFromObject('Service__c').values()){
				recordTypeList.add(new RecordType(id = recorType.getRecordTypeId(), Name = recorType.getName()));
			}
		}

    	VisualEditor.DynamicPickListRows recordTypes = new VisualEditor.DynamicPickListRows();

    	recordTypes.addRow(defaultValues);

    	for (RecordType rt : recordTypeList) {
    		VisualEditor.DataRow rtvalue = new VisualEditor.DataRow(rt.Name, String.valueOf(rt.Id));
    		recordTypes.addRow(rtvalue);
    	}

    	return recordTypes;
 
    }

}