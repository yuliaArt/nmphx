public class CaseNoteController  {

	public static List<Service__c> records {get; set;}
	public static String currentObjectRecordName {get; set;}
	public static String startDate {get;set;}
	public static String endDate {get;set;}

	public CaseNoteController(){
		String recordId = ApexPages.currentPage().getParameters().get('recordId');
		String minDate = ApexPages.currentPage().getParameters().get('startDate');
		String maxDate = ApexPages.currentPage().getParameters().get('endDate');
		String selectedRecordType = ApexPages.currentPage().getParameters().get('selectedRecordType');
		startDate = minDate;
		endDate = maxDate;

		records = getRecords(recordId, minDate, maxDate, selectedRecordType).records;
		currentObjectRecordName = getCurrentObjectRecordName(recordId);
	}

	public static String getCurrentObjectRecordName(String recordId){
		String currentObjectRecordName = [SELECT Name FROM Program_Enrollment__c WHERE Id =: recordId].Name;
		return currentObjectRecordName;
	}

	@AuraEnabled
	public static DataWrapper getRecords(String recordId, String startDate, String endDate, String selectedRecordType){ 
		List<RecordType> recordTypeValues = new List<RecordType>();
		List<Service__c> services = new List<Service__c>();

		Map<String, Schema.RecordTypeInfo> allRecordTypesFromObject = ObjectUtils.getAllRecordTypesFromObject('Service__c');	
		for (Schema.RecordTypeInfo recordType : allRecordTypesFromObject.values()) {
			if (!recordType.isMaster() && recordType.isActive() && recordType.isAvailable()) {
				recordTypeValues.add(new RecordType(Id = recordType.getRecordTypeId(), Name = recordType.getName()));
			}
		}
		
		String filters = ' ';
		if(String.isNotBlank(startDate) && startDate != 'undefined'){
			filters += ' AND Date_of_Service__c >= '+startDate;
		}
		if(String.isNotBlank(endDate) && endDate != 'undefined'){
			filters += ' AND Date_of_Service__c <= '+endDate;
		}
		if(String.isNotBlank(selectedRecordType) && selectedRecordType != 'undefined'){
			filters += ' AND RecordTypeId = \''+selectedRecordType+'\'';
		}	 
		String query = ObjectUtils.getAllFields('Service__c')+', createdBy.Name FROM Service__c WHERE Program_Enrollment__c =:recordId '+ filters;
		services = Database.query(query);
	
		DataWrapper dataWrapper = new DataWrapper();
		dataWrapper.recodTypeValues = recordTypeValues; 
		dataWrapper.records = services;

		return dataWrapper;
	}

	public class DataWrapper{ 
		@AuraEnabled public List<Service__c> records {get; set;}
		@AuraEnabled public List<RecordType> recodTypeValues {get; set;}
	}
}