global class BatchExceptionLogDeleteOld implements Database.Batchable<SObject> {

	global Database.QueryLocator start(Database.BatchableContext context) {
		return Database.getQueryLocator('SELECT Id, Name FROM Exception_Log__c WHERE CreatedDate < LAST_N_MONTHS:2');
	}

	global void execute(Database.BatchableContext context, List<Exception_Log__c> scope) {
		List<Database.DeleteResult> deleteResults = Database.delete(scope);
		System.debug('deleteResults ' + deleteResults);
	}

	global void finish(Database.BatchableContext context) {

	}
}