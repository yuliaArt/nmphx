@isTest
public class EventAttendanceControllerTest {

 @TestSetup
    public static void setup(){
        TEstFactory.eventAttendanceontrollerTest();
    }

    public static  testmethod void attendancetestMethod(){

        Birdseye_Event__c eventBirdseye =  [select Id, Name from Birdseye_Event__c where Name LIKE '%Test Name'];
        List<Event_Participant__c> eventAttendances =  [select Id, Name, Birdseye_Event__c from Event_Participant__c where Birdseye_Event__c =: eventBirdseye.Id ];


        Test.startTest();
        EventAttendanceController.serverGetEventParticipantRecords(String.valueOf(eventBirdseye.Id));
        Test.stopTest();

        List<Event_Participant__c> eventAttendancesAfter =  [select Id, Name, Birdseye_Event__c from Event_Participant__c where Birdseye_Event__c =: eventBirdseye.Id ];

        System.assertEquals(eventAttendances.size(), eventAttendancesAfter.size());
    }
    public static  testmethod void attendanceUpdatetestMethod(){

        Birdseye_Event__c eventBirdseye =  [select Id, Name from Birdseye_Event__c where Name LIKE '%Test Name'];

        List<Event_Participant__c> eventAttendances =  [select Id, Name, Birdseye_Event__c from Event_Participant__c where Birdseye_Event__c =: eventBirdseye.Id ];

        Test.startTest();
        EventAttendanceController.serverUpdateParticipantRecord(eventAttendances);
        Test.stopTest();
    }
}