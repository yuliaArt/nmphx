public with sharing class PrintableViewServicePlanController {
    Service_Plan__c servicePlans { get; set; }
    List<SObject> goalsWithActionSteps { get; set; }
    Id recordId;
    String recordType {get; set;}
    public PrintableViewServicePlanController() {
        this.recordId= ApexPages.currentPage().getParameters().get('recordId');
        this.recordType=getRecordType();
        this.servicePlans = getServicePlans();            
        this.goalsWithActionSteps=getGoalsWithActionSteps();          
    }
    public List<Schema.FieldSetMember> getFields(){
        if(recordType=='ISP')
        return SObjectType.Service_Plan__c.FieldSets.ISP_Fields.getFields();
        else return SObjectType.Service_Plan__c.FieldSets.Family_Support_Fields.getFields();
    }
    public Service_Plan__c getServicePlans() {
        String query = 'SELECT ';
        for(Schema.FieldSetMember f : this.getFields()) {

            query += f.getFieldPath() + ', ';

        }
        query += 'Id, Name FROM Service_Plan__c WHERE Id=:recordId LIMIT 1';
        return Database.query(query);
    }
    public String getRecordType()
    {
        return [SELECT Id,RecordType.Name FROM Service_Plan__c WHERE Id=:recordId].RecordType.Name;
    }
    public List<SObject> getGoalsWithActionSteps()
    {
        return [SELECT id,
										  Name,
										  Service_Plan__c,
										  Start_Date__c,
										  End_Date__c,
										  Active__c,
			    						  Outcome__c,
										  Notes__c,
                                          How_will_we_know_the_goal_has_been_met__c,
                                            (SELECT id, 
												    Name,  
												    Start_Date__c,
												    Completion_Date__c,
												    Notes__c,
												    Due_Date__c,
                                                    Who_is_Responsible__c,
												    Goal__c 
                                             FROM Action_Steps__r) 
                                     FROM Goal__c 
                                     WHERE Service_Plan__c =: recordId
        ]; 
    }

}