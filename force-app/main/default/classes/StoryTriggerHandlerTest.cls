@IsTest
public class StoryTriggerHandlerTest {
    
	@Testsetup
    public static void setup(){
        Trigger_Settings__c disableTrigger = (Trigger_Settings__c) TestFactory.createSObject(new Trigger_Settings__c(), 'TestFactory.Trigger_SettingsDefaults', true);          

    }
    
    @IsTest static void shareRecordTestUpdate(){
        
        Story__c str = new Story__c();
        str.Story__c = 'Lorem Ipsum Story';
        insert str;
        
        //Story__c str = [Select Id, Share__c from Story__c limit 1];
        str.Share__c = true;
        update str;
        
        List<Story__Share> sharesList = [Select Id from Story__Share where AccessLevel = 'Read'];
        System.assertEquals(true, sharesList.size() > 1);
        
    }
    
    @IsTest static void shareRecordTestInsert(){
        
        Story__c str = new Story__c();
        str.Story__c = 'Lorem Ipsum Story';
        str.Share__c = true;
        insert str;
        
        List<Story__Share> sharesList = [Select Id from Story__Share where AccessLevel = 'Read'];
        System.assertEquals(true, sharesList.size() > 1);
        
    }

}