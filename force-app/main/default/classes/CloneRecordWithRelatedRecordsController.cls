public class CloneRecordWithRelatedRecordsController  {

    @AuraEnabled
    public static id cloneMethod(String recordId){

        // Map to copy API Name of fileds by API name 
        Map<String, String> mapRelatedListToCopy = new Map<String, String>();
        //select name from custom metadata
        List<CloneRecordWithRelatedRecords__mdt> customMetadataToCopy = [SELECT ObjectAPINameToCopy__c FROM CloneRecordWithRelatedRecords__mdt WHERE isVisiable__c = true];      
       //  add name of fields to copy
        for(CloneRecordWithRelatedRecords__mdt mtd : customMetadataToCopy){
            mapRelatedListToCopy.put(mtd.ObjectAPINameToCopy__c,mtd.ObjectAPINameToCopy__c);
        }


        Id objectId = Id.valueOf(recordId);
        Map<String,String> reletedListMap = ObjectUtils.getReletedObjects(objectId);
        String ObjectName = objectId.getSObjectType().getDescribe().getName();
        String whereClauseStr= 'Id= \'' + objectId + '\''  ;

        string soqlQueryStr = getCreatableFieldsSOQL(ObjectName)+ ' from '+ ObjectName + ' where ' + whereClauseStr;
        sObject sObjectRecord = Database.query(soqlQueryStr);
        
        sObject newsObjectRecord = sObjectRecord.clone(false, true); //do a deep clone
        insert newsObjectRecord;
               
        List<String> mappedListToClone = new List<String>();     

         for(String sObjectName : reletedListMap.keySet()){
             system.debug(sObjectName);
             if(sObjectName ==  mapRelatedListToCopy.get(sObjectName)){
                  mappedListToClone.add(sObjectName);
             }
         }

        List<sObject> queryListsObjects = new List<sObject>();
       
        //reusing apiname parent
        String parantName = ObjectName;
        //create id for search
        String idParantName = '=\'' + objectId + '\'' ;
       

       //add releted objects to copy
       // query have to be in foreach, because we need to query All fields for related records, and the change API name parent 
        for(String sObjectName : mappedListToClone){
            String search = getCreatableFieldsSOQL(sObjectName)+' from '+ sObjectName + ' where ' + parantName + idParantName;
            queryListsObjects.addAll(Database.query(search));
        }

        //list to insert and copy
        List<sObject> toInsert = new List<sObject>();

        //copy releted records to list
        for(sObject s : queryListsObjects){
            sObject sClone = s.clone(false, true);  
            sClone.put(ObjectName, newsObjectRecord.id);
            toInsert.add(sClone);
        }  

        insert toInsert;
        return newsObjectRecord.id;

    }
    
	//This method will return the all fields from the Object dynamically
    public static  string getCreatableFieldsSOQL(String objectName){
            String selects = '';
            
            // Get a map of field name and field token
            Map<String, Schema.SObjectField> fMap = Schema.getGlobalDescribe().get(objectName.toLowerCase()).getDescribe().Fields.getMap();
            list<string> selectFields = new list<string>();
            //Null check if fMap is not equals to null
            if (fMap != null){
                for (Schema.SObjectField ft : fMap.values()){ // loop through all field tokens (ft)
                    Schema.DescribeFieldResult fd = ft.getDescribe(); // describe each field (fd)
                    if (fd.isCreateable()){ // field is creatable
                        selectFields.add(fd.getName());
                    }
                }
            }
            if (!selectFields.isEmpty()){
                for (string s:selectFields){
                //Fields are sperated by ','
                    selects += s + ',';
                }
                if (selects.endsWith(',')){selects = selects.substring(0,selects.lastIndexOf(','));}
                
            }
            //method will return Select which will be used in query and further upend with from and where
            return 'SELECT ' + selects  ;
    }
           
}