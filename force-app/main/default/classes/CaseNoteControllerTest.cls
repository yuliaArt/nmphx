@IsTest
private class CaseNoteControllerTest  {

	@TestSetup
	private static void testSetup() {
		TestFactory.createDataForCaseNotePreviewController();
	}

	@IsTest
	private static void getRecordsTest(){
		Program_Enrollment__c programEnrollment = [SELECT Id FROM Program_Enrollment__c LIMIT 1];
		
		ApexPages.currentPage().getParameters().put('recordId',programEnrollment.Id);
		ApexPages.currentPage().getParameters().put('startDate', ''+Datetime.now().format('yyyy-MM-dd'));
		ApexPages.currentPage().getParameters().put('endDate', ''+Datetime.now().addDays(1).format('yyyy-MM-dd'));

		CaseNoteController caseNoteController = new CaseNoteController();
	}
}