/*
* Provisio Partners
* Developer: Yuliia Artemenko <yuliia.artemenko@redtag.com.ua>
* Description:
* 	Place all OBJECT OPERATION trigger actions in this file to
* 	allow for easy deduction of execution order
*/

trigger TDTM_BedAssignmentTrigger on Bed_Assignment__c (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
    // Checking if trigger is not disabled in Custom Settings
    List<Trigger_Settings__c> triggerSet = [Select Id, Disable_BedAssignment__c from Trigger_Settings__c Limit 1];
    
	if (triggerSet[0].Disable_BedAssignment__c != true){
        BedAssignmentTriggerHandler.entry(new TriggerParams(trigger.isBefore, trigger.isAfter, trigger.isInsert, trigger.isUpdate, trigger.isDelete, trigger.isUndelete, trigger.new, trigger.old, trigger.newMap, trigger.oldMap));
    }
}