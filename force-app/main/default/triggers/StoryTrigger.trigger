trigger StoryTrigger on Story__c (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
	// Checking if trigger is not disabled in Custom Settings
	List<Trigger_Settings__c> triggerSet = [Select Id, Disable_StoryTrigger__c from Trigger_Settings__c Limit 1];
	if (triggerSet[0].Disable_StoryTrigger__c != true){
		StoryTriggerHandler.entry(new TriggerParams(trigger.isBefore, trigger.isAfter, trigger.isInsert, trigger.isUpdate, trigger.isDelete, 
			trigger.isUndelete, trigger.new, trigger.old, trigger.newMap, trigger.oldMap));
	}
}