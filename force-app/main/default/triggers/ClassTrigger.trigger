//**
// Author: Mykhailo Senyuk
// Date: May 08, 2019
//
// This code is the property of Provisio Partners and copy or reuse is prohibited.
// Copyright @2018 Provisio Partners Illinois Ltd. All Rights Reserved.
// support@provisiopartners.org
//**
trigger ClassTrigger on Class__c (before insert, before update, before delete, after insert, after update, after delete, after undelete)  { 
	// Checking if trigger is not disabled in Custom Settings
	List<Trigger_Settings__c> triggerSet = [Select Id, Disable_ClassTrigger__c from Trigger_Settings__c Limit 1];
	
	if (triggerSet[0].Disable_ClassTrigger__c != true){
		ClassTriggerHandler.entry(new TriggerParams(trigger.isBefore, trigger.isAfter, trigger.isInsert, trigger.isUpdate, trigger.isDelete,trigger.isUndelete, trigger.new, trigger.old, trigger.newMap, trigger.oldMap));
	}
}