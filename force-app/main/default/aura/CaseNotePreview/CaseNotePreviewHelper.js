({
	getRecords: function (component, event, helper) {
		component.set('v.spinner',true);
		
		var recordId = component.get('v.recordId');
		var startDate = component.get('v.startDate');
		var endDate = component.get('v.endDate');
		var selectedRecordType = component.get('v.selectedRecordType');

		var action = component.get('c.getRecords');
		action.setParams({
            'recordId': recordId,
			'startDate': startDate,
			'endDate': endDate,
			'selectedRecordType': selectedRecordType
        });
		action.setCallback(this, function (a) {
            switch (a.getState()) {
                case "SUCCESS":
					console.log('success');
                    var response = a.getReturnValue();			

					component.set('v.recordTypeValues', response.recodTypeValues);
					component.set('v.records', response.records);
					component.set('v.spinner',false);
                    break;
                case "ERROR":
                    var resultsToast = $A.get("e.force:showToast");
                    resultsToast.setParams({
                        "type": "Error",
                        "title": "Error",
                        "message": "Unable to load Data"
                    });
                    resultsToast.fire();
                    console.log('Error in calling getStartData:\n' + a.getError()[0].message);
					component.set('v.spinner',false);
					
                    break;
                default:
                    console.log('Unhandled problem in calling getStartData.');
            }
        });
        $A.enqueueAction(action);
	},

	referenceToPrintPage: function(component, event, helper){
		var recordId = component.get('v.recordId');
		var startDate = component.get('v.startDate');
		var endDate = component.get('v.endDate');
		var selectedRecordType = component.get('v.selectedRecordType');
		
		var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
          "url": "/apex/CaseNotePrintPage?recordId=" + recordId + "&startDate=" + startDate + "&endDate=" + endDate + "&selectedRecordType=" + selectedRecordType,
          "isredirect": "true"
        });
        urlEvent.fire();
	},
})