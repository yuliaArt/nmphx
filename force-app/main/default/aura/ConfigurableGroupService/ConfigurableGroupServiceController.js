({
	 doInit: function (component, event, helper) {
        helper.getServiceWrapper(component,helper);

       if (component.get("v.firstFieldsApi") != undefined ) {
            helper.getFields(component, event, "v.firstFieldsApi", "v.firstFieldsList");
        }
        if (component.get("v.secondFieldsApi") != undefined ) {
            helper.getFields(component, event, "v.secondFieldsApi", "v.secondFieldsList");
        }
        if (component.get("v.thirdFieldsApi") != undefined ) {
            helper.getFields(component, event, "v.thirdFieldsApi", "v.thirdFieldsList");
        }
     },
    callprogram: function (component, event, helper) {
        helper.getContactWrappers(component, event, helper);
         
    },
    toggleBoth: function (component, event, helper) {
        helper.toggleSection(component, event, "serviceFields");
        helper.toggleSection(component, event, "selectContacts");
    },
    toggleServiceFields: function (component, event, helper) {
        helper.toggleSection(component, event, "serviceFields");
    },
    toggleSelectContacts: function (component, event, helper) {
        helper.toggleSection(component, event, "selectContacts");
    },
    clearSelected: function (component, event, helper) {
        helper.clearSelected(component);
    },
    checkAll: function (component, event, helper) {
        helper.checkAll(component);
    },
    rerender: function (component, event) {
        if(event.which === 13) {
            event.preventDefault();
            component.set("v.searchTerm", component.get("v.searchString"));
            component.set("v.contactWrapper", component.get("v.contactWrapper"));
        }
    },
    onRecordSubmit: function(component, event, helper) {

        event.preventDefault();
        helper.insertServices(component, event, helper);

    }
 })