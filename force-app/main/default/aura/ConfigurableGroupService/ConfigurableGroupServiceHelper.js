({
	getContactWrappers : function(component) {

        component.set("v.spinner", true);
        var emptyList = [];
        component.set("v.contactWrapper", emptyList);

        var action = component.get("c.getProgramEnrollments");
        var selected = component.get("v.selectedProgram");
        var groupfamiliesId = component.get("v.group.Id");
        action.setParams({ 
           "programSelectedValue": selected,
           "groupId":groupfamiliesId
        });

		action.setCallback(this,function(a){
            //get the response state
            var state = a.getState();

            //check if result is successfull
            if(state == "SUCCESS"){

                var result = a.getReturnValue();
                if(!$A.util.isEmpty(result) && !$A.util.isUndefined(result))
                    
                    component.set("v.contactWrapper", result);

                    if (result == null || result.length == 0) {
                        component.set('v.message', 'No values for selected program.');
                    } else {
                        component.set('v.message', '');
                    }

            } else if(state == "ERROR"){

                var errors = a.getError();
                alert('Error in calling server side action for ContactWrapper.' +
                errors[0].message);
            }
            component.set("v.spinner", false);
        });

        //adds the server-side action to the queue
        $A.enqueueAction(action);
    },
    getFields: function(component, event, fieldsApi, feildsList) {

        var feildsRow = component.get(fieldsApi);
        var action = component.get("c.convertStrToList");

        action.setParams({
            'str': feildsRow
        });

        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var fieldsMap = [];

                var result = response.getReturnValue();
                for(var key in result){
                    fieldsMap.push({ value: result[key] });
                }
                component.set(feildsList, fieldsMap);

            } else {
                console.log('error');
            }
        });
        $A.enqueueAction(action);

    },
    clearSelected : function(component) {
        var ContactWrappers = component.get('v.contactWrapper');

        for(var i in ContactWrappers) {
            ContactWrappers[i].isSelected = false;
        }

        component.set('v.contactWrapper', ContactWrappers);
    },

    getServiceWrapper : function(component) {
        var action = component.get("c.newServiceWrapper");

        action.setCallback(this,function(a){
            //get the response state
            var state = a.getState();

            //check if result is successfull
            if(state == "SUCCESS"){
                var result = a.getReturnValue();
                if(!$A.util.isEmpty(result) && !$A.util.isUndefined(result))
                    component.set("v.serviceWrapper",result);
            } else if(state == "ERROR"){
                alert('Error in calling server side action for ServiceWrapper');
            }
        });
          $A.enqueueAction(action);

    },
    checkAll : function(component) {
        var ContactWrappers = component.get('v.contactWrapper');
        if(component.get('v.checkboxAll')){
            component.set("v.checkboxAll", false);
            for(var i in ContactWrappers) {
                ContactWrappers[i].isSelected = false;
            }
        } else {
            component.set("v.checkboxAll", true);
            for(var i in ContactWrappers) {
                ContactWrappers[i].isSelected = true;
            }
        }
        component.set('v.contactWrapper', ContactWrappers);
    },
    toggleSection : function(component, event, sectionId) {
        var acc = component.find(sectionId);
        for(var cmp in acc) {
            $A.util.toggleClass(acc[cmp], 'slds-hide');
            $A.util.toggleClass(acc[cmp], 'slds-show');
       }
    },
    insertServices : function(component, event) {
        
        var eventFields = event.getParam("fields");
        var contactWrapper = component.get("v.contactWrapper");
	
        var oneIsSelected = false;

        if (!$A.util.isEmpty(contactWrapper) && !$A.util.isUndefined(contactWrapper)) {
            for (var cnt in contactWrapper) {
                if (contactWrapper[cnt].isSelected) {
                    oneIsSelected = true;
                }
            }
        }
        
        if (oneIsSelected) {
            component.set("v.spinner", true);
            var action = component.get("c.insertRecords");
            action.setParams({
                'contactWrappers' : JSON.stringify(contactWrapper),
                'eventFields' :  JSON.stringify(eventFields),
                'groupId' : component.get("v.group.Id"),
                'rcdTypeId' : component.get("v.recordTypeName"),
                'objectAPIName' : component.get("v.objectAPIName")
            });

            action.setCallback(this, function (a) {
                //get the response state
                var state = a.getState();
                if (state == "SUCCESS") {

                    var resultsToast = $A.get("e.force:showToast");
                    resultsToast.setParams({
                        "type": "Success",
                        "title": "Success",
                        "message": "Records were created!"
                    });
                    resultsToast.fire();
                    component.set("v.spinner", false);

                    // $A.get('event.force:refreshView').fire();
                    document.location.reload()

                } else if (state == "ERROR") {
                    var errors = a.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            this.showToast(component, event, "Error", errors[0].message, "Error"); 
                        }
                    }
                    component.set("v.message", JSON.stringify(errors));
                }
                
                component.set("v.spinner", false);
            });
            $A.enqueueAction(action);
            
       	} else {            
            this.showToast(component, event, "Error", "Please select services you want to create.", "Error");
        }
    },
    showToast : function(component, event, title, message, type) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "duration" : 500,
            "type": type,
            "message" : message,
            "title" : title
        });
        toastEvent.fire();
    },
})