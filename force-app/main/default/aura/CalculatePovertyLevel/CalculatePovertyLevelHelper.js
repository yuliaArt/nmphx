({
	calculatePovertyLevel: function (component, helper) {
		component.set('v.spinner',true);
		console.log('recordId '+component.get('v.recordId'));
		var action = component.get('c.povertyLevelCalculation');
        action.setParams({
            'recordId': component.get('v.recordId')
        });
        action.setCallback(this, function (response) {
            switch (response.getState()) {
                case "SUCCESS":
					if(response.getReturnValue() != "SUCCESS"){
						$A.get("e.force:closeQuickAction").fire();
						var resultsToast = $A.get("e.force:showToast");
						resultsToast.setParams({
							"type": "Error",
							"title": "Error",
							"message": response.getReturnValue()
						});
						resultsToast.fire();
					} else {
						$A.get("e.force:closeQuickAction").fire();
						$A.get('e.force:refreshView').fire();
					}
                    break;
                case "ERROR":
					$A.get("e.force:closeQuickAction").fire();
                    var resultsToast = $A.get("e.force:showToast");
                    resultsToast.setParams({
                        "type": "Error",
                        "title": "Error",
                        "message": "Unable to calculate poverty level"
                    });
                    resultsToast.fire();
                    console.log('Error in calling calcPovertyLevel:\n' + response.getError()[0].message);
                    break;
                default:
                    console.log('Unhandled problem in calling calcPovertyLevel.');
            }
        });
        $A.enqueueAction(action);
	},
	
})