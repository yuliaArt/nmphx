<!--
Author: Pulkit Nautiyal
Date: January 26, 2018
Description: Lightning Component to update attendance
    Revised:
    Author: Yaroslav Mazuryk
    Date: January 3, 2019
    Description: Code was modified due to a different data model.

This code is the property of Provisio Partners and copy or reuse is prohibited.
Copyright @2018 Provisio Partners Illinois Ltd. All Rights Reserved.
support@provisiopartners.org
-->

<aura:component description="Attendance" controller="DailyAttendanceController"
                implements="force:lightningQuickActionWithoutHeader,force:hasRecordId,flexipage:availableForAllPageTypes">
    <!-- Attribute for Attendance List -->
    <aura:attribute name="attendanceList" type="Attendance__c[]"/>
	<aura:attribute name="attendanceListFieldName" type="DailyAttendanceSetting__mdt[]"/>
	<aura:attribute name="attendanceListFieldNameHeader" type="DailyAttendanceSetting__mdt[]"/>

    <aura:attribute name="date" type="Date"/>
	
	<!-- Attribute for page width -->
	<aura:attribute name="divWidth" type="Integer" default="600" />

    <!-- Do Init method-->
    <aura:handler name="init" value="{!this}" action="{!c.doInit}"/>

    <aura:registerEvent name="DailyAttendanceChangeValueEvent" type="c:DailyAttendanceChangeValueEvent"/>
	
	<!-- Eventh methods -->	
	<aura:handler event="c:DailyAttendanceChangeValueEvent"  action="{!c.setValue}"/>
	<aura:handler event="c:DailyAttendanceChangeValueEvent"  action="{!c.setValueByColumn}"/>

	<aura:attribute name="toggle" type="Boolean" default="false"/>
	<aura:if isTrue="{!v.toggle}">
        <div class="slds-is-relative">
            <lightning:spinner variant="brand" size="large" aura:id="mySpinner" style="height: 50vh"/>
        </div>
    </aura:if>

    <!--Page start -->
    <aura:html tag="style">
		.slds-modal__container{
	        width:{!v.divWidth+'px !important;'}
			max-width: {!v.divWidth+'px !important;'}
		}
	</aura:html>
    <!-- First section header -->
    <div class="slds-box slds-p-around_medium">
        <div class="" style="cursor: pointer;padding: 0rem;" >
            <section class="slds-clearfix">
                <header class="slds-modal__header">
                    <h2 class="slds-text-heading_medium">Daily Attendance</h2>
                </header>
            </section>
        </div>
    </div>

    <div style="overflow-y: hidden !important; margin-right: -14px;margin-left: -11px;" class="slds-modal__content slds-p-around_medium " id="modal-content-id-1">

		<div class="slds-grid slds-wrap slds-grid--align-space" Id="header" >
			<div class="slds-col--padded slds-size_1-of-2 slds-m-bottom_small">
				<ui:inputDate value="{!v.date}" change="{!c.getAttendanceRecords}" displayDatePicker="true" class="myDatePicker"/>
			</div>
			<aura:if isTrue="{!and(!empty(v.attendanceList),!empty(v.attendanceListFieldNameHeader) )}">
 
				<aura:iteration items="{!v.attendanceListFieldNameHeader}" var="attendanceField">
					<div class="slds-col--padded slds-size_1-of-2 slds-m-bottom_small">                                                                    
						<center><c:GetFieldType objectName="{!attendanceField.Object_API_Name__c}" fieldApiName="{!attendanceField.Field_API_Name__c}" fieldName="{!attendanceField.MasterLabel}" isHeader="TRUE" /></center>
					</div>
				</aura:iteration>

			</aura:if>
        </div>        
        
        <!-- If Attendance list is blank, Message show-->
        <aura:if isTrue="{!empty(v.attendanceList)}">
            <div class="slds-p-around_medium slds-align_absolute-center">
                <h3>There are no attendances for this day</h3>
            </div>
        </aura:if>
		
        <!--If Attendance list has records, Show the records in the table -->

        <div aura:id="table" class="slds-scrollable" style="overflow: auto;" >
            <aura:if isTrue="{!!empty(v.attendanceList)}" >
                <table class="slds-table slds-table_bordered slds-table_resizable-cols" aura:id="tab" role="grid">
                    <thead>
                        <tr class="headerFirstLine">
                            <th aria-label="Client Name" aria-sort="none" class="slds-text-title_caps slds-is-resizable slds-is-sortable" scope="col" RowSpan="2">
                                <div class="slds-truncate slds-text-align_center slds-p-vertical_x-small" title="Client Name">Client Name</div>
                            </th>
							<aura:iteration var="attendanceField" items="{!v.attendanceListFieldName}">
								<th aria-label="{!attendanceField.MasterLabel}" aria-sort="none" class="slds-text-title_caps slds-is-resizable slds-is-sortable" scope="col" Style="vertical-align: middle;">
									<div class="slds-truncate slds-text-align_center slds-p-vertical_x-small" title="{!attendanceField.MasterLabel}">{!attendanceField.MasterLabel}</div>
								</th>
							</aura:iteration>
                        </tr>
						 <tr class="headerSecondLine">
                           
							<aura:iteration var="attendanceFieldName" items="{!v.attendanceListFieldName}">
								<th class="slds-hint-parent">
									 <div class="slds-truncate slds-p-vertical_xxx-small slds-p-horizontal_x-small" >
										<center><c:GetFieldType objectName="{!attendanceFieldName.Object_API_Name__c}" fieldName="{!attendanceFieldName.MasterLabel}" fieldApiName="{!attendanceFieldName.Field_API_Name__c}" isHeader="TRUE" /></center>
									</div>
								</th>
							</aura:iteration>
						</tr>
<!--
-->
                    </thead>

                    <tbody>
						
                        <aura:iteration var="attendance" items="{!v.attendanceList}">
                            <tr aria-selected="false" class="slds-hint-parent">
                                <th scope="row" tabindex="0">
                                    <div class="slds-truncate" title="{!attendance.Bonus_Participant_For__r.Name}">
                                        <center><ui:outputText value="{!attendance.Class_Roster__r.Contact__r.Name}"/></center>
									</div>
                                </th>
								<aura:iteration var="attendanceField" items="{!v.attendanceListFieldName}">
									<td class="slds-hint-parent">
										<div class="slds-truncate " title="{!attendanceField.MasterLabel}">
											<center><c:GetFieldType objectName="{!attendanceField.Object_API_Name__c}" fieldName="{!attendanceField.MasterLabel}" fieldApiName="{!attendanceField.Field_API_Name__c}" record="{!attendance}"/></center>
										</div>
									</td>
								</aura:iteration>
                            </tr>
                        </aura:iteration>
                    </tbody>

                </table>
            </aura:if>
        </div>

        <!--Buttons at the bottom of the Popup modal -->
    </div>
    <div style="margin-top: 32px;">
        <center>
            <lightning:button variant="neutral" label="Cancel" onclick="{!c.closeWindow}"/>
            <aura:if isTrue="{!!empty(v.attendanceList)}">
                <lightning:button variant="brand" label="Submit" onclick="{!c.handleClick}"/>
            </aura:if>
        </center>
    </div>
    
</aura:component>