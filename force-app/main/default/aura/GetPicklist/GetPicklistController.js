/*
* Provisio Partners
* Author : Pulkit Nautiyal
* Description : LEX component to get picklist
* Created Date : March 26th 2018.
*/
    
({
    doInit: function(component, event, helper) {
        helper.fetchPickListVal(component);
    },

	valueByHeared: function(component, event, helper){
		helper.valueByHeared(component);
	},

	recordValue: function(component, event, helper){
		helper.recordValue(component);
	}

})