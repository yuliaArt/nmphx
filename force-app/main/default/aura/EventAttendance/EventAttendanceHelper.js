({
	getEvParticipantRecords: function (component) {
		component.set('v.toggle',true);
		var action = component.get('c.serverGetEventParticipantRecords');
		
        action.setParams({
            'eventId': component.get('v.recordId')
        });

        action.setCallback(this, function (a) {
            switch (a.getState()) {
                case "SUCCESS":
                    var response = a.getReturnValue();
                    component.set('v.attendanceList', response);
					if(response == null){
					}
						component.set('v.toggle',false);
                    break;
                case "ERROR":
                    var resultsToast = $A.get("e.force:showToast");
                    resultsToast.setParams({
                        "type": "Error",
                        "title": "Error",
                        "message": "Unable to load attendances"
                    });
                    resultsToast.fire();
                    console.log('Error in calling serverGetEventParticipantRecords:\n' +
                        a.getError()[0].message);
					component.set('v.toggle',false);
                    break;
                default:
                    console.log('Unhandled problem in calling serverGetEventParticipantRecords.');
            }
        });
        $A.enqueueAction(action);
    },

	getEvParticipantFieldName: function(component){
		var attendanceListFieldNameHeader = component.get('v.attendanceListFieldNameHeader');
		var attendanceListFieldName = component.get('v.attendanceListFieldName');
		var divWidth = component.get('v.divWidth');

		var action = component.get('c.eventAttendanceSettings');
		action.setCallback(this, function(a){
			switch(a.getState()){
				case "SUCCESS":
					var response = a.getReturnValue();
					for(var i in response){
						if(response[i].IsHeader__c){
							attendanceListFieldNameHeader.push(response[i]);
						} else {
							attendanceListFieldName.push(response[i]);
						}
					}
					var width = attendanceListFieldName.length * 100;
					var newWidth = divWidth+width;
					if(newWidth >1200){
						newWidth = 1200;
					}
					component.set('v.divWidth', newWidth);
					component.set('v.attendanceListFieldName', attendanceListFieldName);
					component.set('v.attendanceListFieldNameHeader', attendanceListFieldNameHeader);
					component.set('v.toggle',false);
					
				break;
                case "ERROR":
                    var resultsToast = $A.get("e.force:showToast");
                    resultsToast.setParams({
                        "type": "Error",
                        "title": "Error",
                        "message": "Unable to load Event Attendance settings"
                    });
                    resultsToast.fire();
                    console.log('Error in calling EventAttendanceSettings:\n' +
                        a.getError()[0].message);
                    break;
                default:
                    console.log('Unhandled problem in calling EventAttendanceSettings.');
            }
		});
		$A.enqueueAction(action);
	},
	updateEvParticipantRecords: function (component) {
        var action = component.get("c.serverUpdateParticipantRecord");

        action.setParams({
            "attendanceUpdate": component.get('v.attendanceList')
        });

        action.setCallback(this, function (a) {
            if(a.getState() == "SUCCESS") {
                var resultsToast = $A.get("e.force:showToast");
                resultsToast.setParams({
                    "type": "Success",
                    "title": "Success",
                    "message": "Records were updated!"
                });
                $A.get("e.force:closeQuickAction").fire();
                resultsToast.fire();
            } else if (a.getState() == "Error"){
                    var resultsToast = $A.get("e.force:showToast");
                    resultsToast.setParams({
                        "type": "Error",
                        "title": "Error",
                        "message": "Unable to update records."
                    });
                    $A.get("e.force:closeQuickAction").fire();
                    resultsToast.fire();
                    console.log('Error in calling serverUpdateParticipantRecord:\n' +
                                a.getError()[0].message);
             } else {
                   console.log('Unhandled problem in calling serverUpdateParticipantRecord.');
             }
        });
        $A.enqueueAction(action);
    },
    setValueByColumn: function (component, event) {
        var templist = component.get("v.attendanceList");
		var value = event.getParam('value');
		var fieldApiName = event.getParam("fieldName");
		var recordId = event.getParam("recordId");
		
		if (recordId == undefined){
			for(var i=0; i< templist.length; i++){
				templist[i][fieldApiName] = value;              
			}       
			component.set('v.attendanceList', templist);
        }
    },

	setValue: function(component, event){
		var value = event.getParam('value');
		var fieldApiName = event.getParam("fieldName");
		var recordId = event.getParam("recordId");
		var templist = component.get("v.attendanceList");
       
		for(var i=0; i< templist.length; i++){
			var listItemId = templist[i]['Id'];
			if(listItemId==recordId){
				templist[i][fieldApiName] = value;              
			}
        }       
        component.set('v.attendanceList', templist);
	}
})