({
	doInit: function (component, event, helper) {
		
        helper.getEvParticipantRecords(component);
	
		helper.getEvParticipantFieldName(component);
		

    },
	//Method called when submit for saving record
    handleClick : function(component, event, helper) {
        helper.updateEvParticipantRecords(component,event,helper);
    },
    //Method called on cancel button clicked
    closeWindow : function(component, event, helper) {
        $A.get("e.force:closeQuickAction").fire();
    },
	
    setValueByColumn:function(component, event, helper) {
        console.log('here2');
       helper.setValueByColumn(component, event);
    },
	
	setValue : function(component, event, helper){
        console.log('here23');
		helper.setValue(component, event);
	}
})