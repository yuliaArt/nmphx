({  
    // Load current profile picture
    doinit: function(component) {
		component.set('v.spinner',true);
        var action = component.get("c.getProfilePicture"); 
        action.setParams({
            parentId: component.get("v.recordId"),
        });
        action.setCallback(this, function(a) {
			 switch (a.getState()) {
                case "SUCCESS":
					console.log('success');
					component.set('v.pictureSrc', '/servlet/servlet.FileDownload?file=' + a.getReturnValue());
					window.setTimeout(
						$A.getCallback(function() {
							component.set('v.spinner',false);
						}), 2000
					);                    
                    break;
                case "ERROR":
                    console.log('Error in calling getStartData:\n' + a.getError()[0].message);
					component.set('v.spinner',false);
                    break;
                default:
                    console.log('Unhandled problem in calling getProfilePicture.');
            }

        });
        $A.enqueueAction(action); 
    },
    
    onDragOver: function(component, event) {
        event.preventDefault();
    },

    onDrop: function(component, event, helper) {
		event.stopPropagation();
        event.preventDefault();
        event.dataTransfer.dropEffect = 'copy';
        var files = event.dataTransfer.files;
        if (files.length>1) {
            return alert("You can only upload one profile picture");
        }
        helper.readFile(component, helper, files[0]);
	}
    
})