({
	//doInit
	doInit: function (component, event, helper) {
		console.log('ooo');
		helper.getRecords(component, event, helper);
	},

	//Method called when submit for saving record
    handleClick : function(component, event, helper) {
        helper.referenceToPrintPage(component, event, helper);
    },
  
    //Method called on cancel button clicked
    closeWindow : function(component, event, helper) {
        $A.get("e.force:closeQuickAction").fire();
    }, 
})