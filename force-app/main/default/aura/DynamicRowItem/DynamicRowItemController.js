({
    AddNewRow : function(component, event, helper){
        // fire the AddNewRow Lightning Event
        component.getEvent("AddRowEvent").fire();
    },

    removeRow : function(component, event, helper){
        // fire the DeleteRowEvent Lightning Event and pass the deleted Row Index to Event parameter/attribute
        component.getEvent("DeleteRowEvent").setParams({"indexVar" : component.get("v.rowIndex") }).fire();
    }

})