({
    doInit : function(component, event, helper) {
        helper.getPlaceholderText(component, event, helper);
        helper.getIcon(component);
    },
    itemSelected : function(component, event, helper) {
        helper.itemSelected(component, event, helper);
    },
    serverCall :  function(component, event, helper) {
        helper.serverCall(component, event, helper);
    },
    clearSelection : function(component, event, helper){
        helper.clearSelection(component, event, helper);
    },
})